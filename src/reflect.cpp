#include <Eigen/Core>

Eigen::Vector3d reflect(const Eigen::Vector3d & in, const Eigen::Vector3d & n)
{
  ////////////////////////////////////////////////////////////////////////////
  // Replace with your code here:
  //equation found here: http://paulbourke.net/geometry/reflected/
  return (in - 2 * n * (in.dot(n))).normalized();
  ////////////////////////////////////////////////////////////////////////////
}
