#include "Sphere.h"
#include "Ray.h"
bool Sphere::intersect(
  const Ray & ray, const double min_t, double & t, Eigen::Vector3d & n) const
{
  ////////////////////////////////////////////////////////////////////////////
  // Replace with your code here:
  Eigen::Vector3d d = ray.direction;
  Eigen::Vector3d e = ray.origin;
  Eigen::Vector3d s = ray.direction;

  //scalars for the quadratic equation
  double a = d.dot(d);
  double b = 2 * e.dot(d);
  double c = e.dot(e) - pow(radius, 2);
 
  //portion under the square root of the quadratic equation
  double under_root = pow(b, 2) - 4 * a * c;


  //error check the quadratic equation, point not on sphere
  if (under_root < 0){
    return false;
  }
  //find both solutions
  else{
    double x1 = (-b + sqrt(under_root)) / (2*a);
    double x2 = (-b - sqrt(under_root)) / (2*a);

    //if statements to chose the smallest result from the quadratic equation that is larger than the min_t
    if(x1 > min_t && x2 > min_t){
      t = std::min(x1, x2);
      Eigen::Vector3d point = e + t * d;
      Eigen::Vector3d normal = point - center;
      normal.normalize();
      n = normal;
      return true;
    }
    else if(x1 > min_t){
      t = x1;
      Eigen::Vector3d point = e + t * d;
      Eigen::Vector3d normal = point - center;
      normal.normalize();
      n = normal;
      return true;
    }
    else if( x2 > min_t){
      t = x2;
      Eigen::Vector3d point = e + t * d;
      Eigen::Vector3d normal = point - center;
      normal.normalize();
      n = normal;
      return true;
    }
    else{
      return false;
    }
  }


  return false;
  ////////////////////////////////////////////////////////////////////////////
}

