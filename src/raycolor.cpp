#include "raycolor.h"
#include "first_hit.h"
#include "blinn_phong_shading.h"
#include "reflect.h"
#include <iostream>

double clamp(double v, double lo, double hi){
    if(v < lo){
      return lo;
    }
    if(v > hi){
      return hi;
    }
    return v;
  }

bool raycolor(
  const Ray & ray, 
  const double min_t,
  const std::vector< std::shared_ptr<Object> > & objects,
  const std::vector< std::shared_ptr<Light> > & lights,
  const int num_recursive_calls,
  Eigen::Vector3d & rgb)
{
  if(num_recursive_calls > 3) return false;
  
  int hit_id; double t; Eigen::Vector3d n;

   if (first_hit(ray, min_t, objects, hit_id, t, n)) {
    //blinn_phong_shading component
    Eigen::Vector3d bps = blinn_phong_shading(ray, hit_id, t, n, objects, lights);
    Ray reflected;
    reflected.direction = reflect(ray.direction.normalized(), n);
    reflected.origin = ray.origin + t*ray.direction;
    Eigen::Vector3d reflectedrgb;
    //reflect component
     if(raycolor(reflected, 0.00000001,objects, lights, num_recursive_calls + 1, reflectedrgb)){
          bps = bps + (objects[hit_id]->material->km.array() * reflectedrgb.array()).matrix();
     }
     double r = clamp(bps[0], 0, 1);
     double g = clamp(bps[1], 0, 1);
     double b = clamp(bps[2], 0, 1);
     rgb = Eigen::Vector3d(r, g, b);
     return true;
   }



  rgb = Eigen::Vector3d(0,0,0);
  return false;
}
