#include "write_ppm.h"
#include <fstream>
#include <cassert>
#include <iostream>

bool write_ppm(
  const std::string & filename,
  const std::vector<unsigned char> & data,
  const int width,
  const int height,
  const int num_channels)
{
    assert((num_channels == 3 || num_channels == 1) && ".ppm only supports RGB or grayscale images");
    
    std::ofstream file(filename, std::ios::binary);
    
    // error handling
    if (!file) {
        return false;
    }
    
    //header for ppm file
    std::string header = "P6\n" + std::to_string(width) + "\n" + std::to_string(height) + "\n" + "255\n";
    file.write(header.c_str(),header.size());


    //write each pixel, if it's gray scale just copy that value 3 times
    for (int i=0; i<width*height*3; i+= 3) {
        if (num_channels == 3) {
            unsigned char r = data[i];
            unsigned char g = data[i + 1];
            unsigned char b = data[i + 2];
            file.write(reinterpret_cast<char *>(&r), sizeof(r));
            file.write(reinterpret_cast<char *>(&g), sizeof(g));
            file.write(reinterpret_cast<char *>(&b), sizeof(b));
        } else {
            unsigned char gray = data[i/3];
            file.write(reinterpret_cast<char *>(&gray), sizeof(gray));
            file.write(reinterpret_cast<char *>(&gray), sizeof(gray));
            file.write(reinterpret_cast<char *>(&gray), sizeof(gray));
        }
    }
    
    file.close();
    // error handling
    if (!file) {
        return false;
    }
    
    return true;
}
